<div class="categorie-header"><span>CATEGORIE</span><h2><?= $title ?></h2></div>
<?php foreach($posts as $post): ?>
<div class="post">
	<div class="header">
		<small class="categorie"><strong><?php echo $post['name']; ?></strong></small>
		<h2><?php echo $post['title']; ?></h2>
		<small class="date"><?php echo $post['created_at']; ?></small>
	</div>
		<div class="photo">
			<img class="post-thumb" src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
		</div>
		<div class="text">
			<?php echo word_limiter ($post['body'], 60); ?>
				<br><br>
			<p><a class="more" href="<?php echo site_url('/posts/'.$post['slug']); ?>">
			Lees meer</a></p>
		</div>
	<hr>
	</div>		 
<?php endforeach; ?>

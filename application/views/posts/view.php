<div class="post">

<div class="header">
	<h2><?php echo $post['title']; ?> </h2>
	<small class="date"><?php echo $post['created_at']; ?></small><br>
</div>
<div class="photo">
	<img class="post-thumb" src="<?php echo base_url(); ?>assets/images/posts/<?php echo $post['post_image']; ?>">
</div>
<div class="text">
	<?php echo $post['body']; ?>
</div>

<hr>
<a class="edit" href="<?php echo base_url(); ?>posts/edit/<?php echo $post['slug']; ?>">Edit</a>

<?php echo form_open('/posts/delete/' .$post['id']); ?>

<input type="submit" value="Delete" id="delete">
</form>
</div>